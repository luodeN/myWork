<?php
namespace app\index\controller;
 
 use think\Controller;
class Index extends Controller
{
    public function lst () 
    {
    	// 从数据库里获取数据
    	$data = db('admin')->select();
    	// 将数据发送到视图html (渲染)
    	$this->assign('data', $data);
    	// 启用视图(启用模板)
    	return $this->fetch();

    }
}
